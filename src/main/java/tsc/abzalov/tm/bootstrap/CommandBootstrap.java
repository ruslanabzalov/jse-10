package tsc.abzalov.tm.bootstrap;

import tsc.abzalov.tm.api.controller.ICommandController;
import tsc.abzalov.tm.api.controller.IProjectController;
import tsc.abzalov.tm.api.controller.ITaskController;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.controller.CommandController;
import tsc.abzalov.tm.controller.ProjectController;
import tsc.abzalov.tm.controller.TaskController;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.service.CommandService;
import tsc.abzalov.tm.service.ProjectService;
import tsc.abzalov.tm.service.TaskService;

import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationArgumentConst.ARG_ARGUMENTS;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;
import static tsc.abzalov.tm.util.Util.INPUT;

public class CommandBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskController taskController = new TaskController(taskService);

    public void run(String... args) {
        System.out.println();
        if (areArgExists(args)) return;

        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");

        String command;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            command = INPUT.nextLine();
            System.out.println();
            if (command == null || command.isEmpty()) continue;
            processCommand(command);
        }
    }

    private boolean areArgExists(String... args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return false;

        processArg(arg);
        return true;
    }

    private void processArg(String arg) {
        switch (arg) {
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_INFO:
                commandController.showInfo();
                break;
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_COMMANDS:
                commandController.showCommandNames();
                break;
            case ARG_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            default:
                commandController.showError(arg, true);
        }
    }

    private void processCommand(String command) {
        switch (command) {
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommandNames();
                break;
            case CMD_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            case CMD_CREATE_PROJECT:
                projectController.createProject();
                break;
            case CMD_LIST_ALL_PROJECTS:
                projectController.listAllProjects();
                break;
            case CMD_LIST_PROJECTS_BY_NAME:
                projectController.listProjectsByName();
                break;
            case CMD_DELETE_ALL_PROJECTS:
                projectController.deleteAllProjects();
                break;
            case CMD_DELETE_PROJECTS_BY_NAME:
                projectController.deleteProjectByName();
                break;
            case CMD_CREATE_TASK:
                taskController.createTask();
                break;
            case CMD_LIST_ALL_TASKS:
                taskController.listAllTasks();
                break;
            case CMD_LIST_TASKS_BY_NAME:
                taskController.listTasksByName();
                break;
            case CMD_DELETE_ALL_TASKS:
                taskController.deleteAllTasks();
                break;
            case CMD_DELETE_TASKS_BY_NAME:
                taskController.deleteTasksByName();
                break;
            case CMD_EXIT:
                commandController.exit();
            default:
                commandController.showError(command, false);
        }
    }

}
