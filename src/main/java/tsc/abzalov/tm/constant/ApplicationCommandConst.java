package tsc.abzalov.tm.constant;

public interface ApplicationCommandConst {

    String CMD_HELP = "help";

    String CMD_INFO = "info";

    String CMD_ABOUT = "about";

    String CMD_VERSION = "version";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

    String CMD_EXIT = "exit";

    String CMD_CREATE_PROJECT = "create-project";

    String CMD_LIST_ALL_PROJECTS = "list-all-projects";

    String CMD_LIST_PROJECTS_BY_NAME = "list-projects-by-name";

    String CMD_DELETE_ALL_PROJECTS = "delete-all-projects";

    String CMD_DELETE_PROJECTS_BY_NAME = "delete-projects-by-name";

    String CMD_CREATE_TASK = "create-task";

    String CMD_LIST_ALL_TASKS = "list-all-tasks";

    String CMD_LIST_TASKS_BY_NAME = "list-tasks-by-name";

    String CMD_DELETE_ALL_TASKS = "delete-all-tasks";

    String CMD_DELETE_TASKS_BY_NAME = "delete-tasks-by-name";

}