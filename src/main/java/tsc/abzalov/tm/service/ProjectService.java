package tsc.abzalov.tm.service;

import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public boolean createProject(Project project) {
        return projectRepository.add(project);
    }

    @Override
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> getProjectsByName(String name) {
        return projectRepository.findByName(name);
    }

    @Override
    public void removeAllProjects() {
        projectRepository.deleteAll();
    }

    @Override
    public boolean removeProjectsByName(String name) {
        return projectRepository.deleteByName(name);
    }

}
