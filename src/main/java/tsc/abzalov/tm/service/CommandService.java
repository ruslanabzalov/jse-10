package tsc.abzalov.tm.service;

import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    public String[] getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

}
