package tsc.abzalov.tm.service;

import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public boolean createTask(Task task) {
        return taskRepository.add(task);
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> getTaskByName(String name) {
        return taskRepository.findByName(name);
    }

    @Override
    public void removeAllTasks() {
        taskRepository.deleteAll();
    }

    @Override
    public boolean removeTaskByName(String name) {
        return taskRepository.deleteByName(name);
    }

}
