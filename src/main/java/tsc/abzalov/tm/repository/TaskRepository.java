package tsc.abzalov.tm.repository;

import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public boolean add(Task task) {
        return tasks.add(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findByName(String name) {
        return tasks.stream().filter(task -> task.getName().equals(name)).collect(Collectors.toList());
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

    @Override
    public boolean deleteByName(String name) {
        return tasks.removeIf(task -> task.getName().equals(name));
    }

}
