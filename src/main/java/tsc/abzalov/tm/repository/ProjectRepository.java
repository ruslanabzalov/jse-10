package tsc.abzalov.tm.repository;

import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public boolean add(Project project) {
        return projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findByName(String name) {
        return projects.stream().filter(project -> project.getName().equals(name)).collect(Collectors.toList());
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

    @Override
    public boolean deleteByName(String name) {
        return projects.removeIf(project -> project.getName().equals(name));
    }

}
