package tsc.abzalov.tm.controller;

import tsc.abzalov.tm.api.controller.ITaskController;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static tsc.abzalov.tm.util.Util.INPUT;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("TASK CREATION\n");
        final String taskName = getTaskName();

        System.out.print("Please, enter task description: ");
        final String taskDescription = INPUT.nextLine();

        final boolean isTaskCreated;
        if (taskDescription == null || taskDescription.isEmpty())
            isTaskCreated = taskService.createTask(new Task(taskName));
        else
            isTaskCreated = taskService.createTask(new Task(taskName, taskDescription));

        if (isTaskCreated)
            System.out.println("Task successfully created.\n");
        else
            System.out.println("Task is not created! Please, try create new task.\n");
    }

    @Override
    public void listAllTasks() {
        System.out.println("ALL TASK LIST\n");
        final List<Task> foundedTasks = taskService.getAllTasks();
        if (foundedTasks.isEmpty())
            System.out.println("Tasks list is empty.");
        else
            foundedTasks.forEach(System.out::println);
        System.out.println();
    }

    @Override
    public void listTasksByName() {
        System.out.println("FIND TASKS BY NAME\n");
        final String taskName = getTaskName();
        System.out.println();

        final List<Task> foundedTasks = taskService.getTaskByName(taskName);
        if (foundedTasks.isEmpty())
            System.out.println("Searched tasks are not found.");
        else
            foundedTasks.forEach(System.out::println);
        System.out.println();
    }

    @Override
    public void deleteAllTasks() {
        System.out.println("TASKS DELETION\n");
        if (taskService.getAllTasks().isEmpty())
            System.out.println("Tasks list is already empty!");
        else {
            taskService.removeAllTasks();
            System.out.println("All tasks successfully deleted.\n");
        }
    }

    @Override
    public void deleteTasksByName() {
        System.out.println("DELETE TASK BY NAME\n");
        final boolean areTasksDeleted = taskService.removeTaskByName(getTaskName());
        if (areTasksDeleted)
            System.out.println("Tasks successfully deleted.\n");
        else
            System.out.println("Tasks are not deleted! Please, check that tasks exist and try again.\n");
    }

    private String getTaskName() {
        System.out.print("Please, enter a task name: ");
        String taskName = INPUT.nextLine();
        while (taskName == null || taskName.isEmpty()) {
            System.out.print("Task name cannot be empty! Please, enter correct task name: ");
            taskName = INPUT.nextLine();
        }
        return taskName;
    }

}
