package tsc.abzalov.tm.controller;

import tsc.abzalov.tm.api.controller.IProjectController;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.model.Project;

import java.util.List;

import static tsc.abzalov.tm.util.Util.INPUT;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("PROJECT CREATION\n");
        final String projectName = getProjectName();

        System.out.print("Please, enter project description: ");
        final String projectDescription = INPUT.nextLine();

        final boolean isProjectCreated;
        if (projectDescription == null || projectDescription.isEmpty())
            isProjectCreated = projectService.createProject(new Project(projectName));
        else
            isProjectCreated = projectService.createProject(new Project(projectName, projectDescription));

        if (isProjectCreated)
            System.out.println("Project successfully created.\n");
        else
            System.out.println("Project is not created! Please, try create new project.\n");
    }

    @Override
    public void listAllProjects() {
        System.out.println("ALL PROJECTS LIST\n");
        final List<Project> foundedProjects = projectService.getAllProjects();
        if (foundedProjects.isEmpty())
            System.out.println("Projects list is empty.");
        else
            foundedProjects.forEach(System.out::println);
        System.out.println();
    }

    @Override
    public void listProjectsByName() {
        System.out.println("FIND PROJECTS BY NAME\n");
        final String projectName = getProjectName();
        System.out.println();

        final List<Project> foundedProjects = projectService.getProjectsByName(projectName);
        if (foundedProjects.isEmpty())
            System.out.println("Searched projects are not found.");
        else
            foundedProjects.forEach(System.out::println);
        System.out.println();
    }

    @Override
    public void deleteAllProjects() {
        System.out.println("PROJECTS DELETION\n");
        if (projectService.getAllProjects().isEmpty())
            System.out.println("Projects list is already empty!");
        else {
            projectService.removeAllProjects();
            System.out.println("All projects successfully deleted.\n");
        }
    }

    @Override
    public void deleteProjectByName() {
        System.out.println("DELETE PROJECT BY NAME\n");
        final boolean areProjectsDeleted = projectService.removeProjectsByName(getProjectName());
        if (areProjectsDeleted)
            System.out.println("Projects successfully deleted.\n");
        else
            System.out.println("Projects are not deleted! Please, check that projects exist and try again.\n");
    }

    private String getProjectName() {
        System.out.print("Please, enter a project name: ");
        String projectName = INPUT.nextLine();
        while (projectName == null || projectName.isEmpty()) {
            System.out.print("Project name cannot be empty! Please, enter correct project name: ");
            projectName = INPUT.nextLine();
        }
        return projectName;
    }

}
