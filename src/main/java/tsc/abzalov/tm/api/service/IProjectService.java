package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    boolean createProject(Project project);

    List<Project> getAllProjects();

    List<Project> getProjectsByName(String name);

    void removeAllProjects();

    boolean removeProjectsByName(String name);

}
