package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    boolean createTask(Task task);

    List<Task> getAllTasks();

    List<Task> getTaskByName(String name);

    void removeAllTasks();

    boolean removeTaskByName(String name);

}
