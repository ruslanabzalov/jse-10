package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Project;

public interface IProjectRepository extends IBaseSubjectRepository<Project> {
}
