package tsc.abzalov.tm.api.repository;

import java.util.List;

public interface IBaseSubjectRepository<T> {

    boolean add(T t);

    List<T> findAll();

    List<T> findByName(String name);

    void deleteAll();

    boolean deleteByName(String name);

}
