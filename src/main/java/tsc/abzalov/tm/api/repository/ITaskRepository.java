package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Task;

public interface ITaskRepository extends IBaseSubjectRepository<Task> {
}
