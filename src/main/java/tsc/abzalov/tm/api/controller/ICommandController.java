package tsc.abzalov.tm.api.controller;

public interface ICommandController {

    void showError(String input, boolean isArg);

    void showHelp();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommandNames();

    void showCommandArgs();

    void exit();

}
