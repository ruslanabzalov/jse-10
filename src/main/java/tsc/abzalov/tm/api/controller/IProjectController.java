package tsc.abzalov.tm.api.controller;

public interface IProjectController {

    void createProject();

    void listAllProjects();

    void listProjectsByName();

    void deleteAllProjects();

    void deleteProjectByName();

}
