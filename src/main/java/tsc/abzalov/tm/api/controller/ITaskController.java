package tsc.abzalov.tm.api.controller;

public interface ITaskController {

    void createTask();

    void listAllTasks();

    void listTasksByName();

    void deleteAllTasks();

    void deleteTasksByName();

}
