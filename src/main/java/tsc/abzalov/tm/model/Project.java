package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Project {

    private final String id = UUID.randomUUID().toString();

    @NotNull
    private String name = "";

    @NotNull
    private String description = "Description is empty.";

    private Project() {
    }

    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.name + ": [ID: " + this.id + ", Description: " + this.description + "]";
    }

}
